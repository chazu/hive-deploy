module.exports = {
    prod: {
        port: 4003,
        host: "{{ ansible_eth0.ipv4.address }}",
        // redis config
        redis_port: '6379',
        redis_host: 'redis1',
        queue_prefix: 'analytics:',
        queue_names: {
            users: 'users',
            one: 'one'
        }
    }
};