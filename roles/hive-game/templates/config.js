/**
 * Example configurations for development and for production.
 * This config file will be changed while deploying, so use it
 * just like an example or doc.
 */
module.exports = {
    prod: {
        port: 5001,
        host: "{{ ansible_eth1.ipv4.address }}",
        db_host: 'mongodb://mongodb1/',
        game_dbs: {
            tictactoe: {
                profiles: {
                    name: 'hive-tictactoe-dev',
                    collection: 'profiles'
                },
                matches: {
                    name: 'hive-tictactoe-dev',
                    collection: 'matches'
                }
            },
            traps: {
                profiles: {
                    name: 'hive-traps-dev',
                    collection: 'profiles'
                },
                matches: {
                    name: 'hive-traps-dev',
                    collection: 'matches'
                }
            }
        },
        external_servers: {
            'hive-main-server': {
                port: 4001,
                ips: ['mainserver1']
            }
        },
        redis_port: '6379',
        redis_host: 'redis1',
        game_config_update_pattern: '*/1 * * * *', // every 3 minute, use a cron pattern here
        queue_prefix: 'queue',
        queue_on_hold_time: 1000 * 20, // the time to wait for a random match till a bot starts to play
        queue_on_hold_time_pattern: '*/20 * * * * *', // cron pattern for on hold matches check
        admin_roles: ['manager', 'support', 'admin'],
        // pusher socket.io settings
        pusher_origins : '*:*',
        pusher_heartbeats: false
    }
};